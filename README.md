Vorlesungsmaterialien des Kurses Statistik für Digital Humanities am Lehrstuhl für Computational Humanities.

Lehrbuch: Andy Field, Jeremy Miles, Zoe Field. Discovering Statistics Using R.

Folien & Vorlesung: Dr. Jochen Tiepmar, https://orcid.org/0000-0002-3866-2830

Lizenz: Public Domain



Folien und Vorlesungsvideos: http://www.informatik.uni-leipzig.de/~jtiepmar/lehre/statdh/ss21/

Youtubeplaylist: https://www.youtube.com/watch?v=BBCitNwFlik&list=PLFiTehFSd4iNMUp03S7aAwBvrR-yY9AFB

Zusatzmaterialien sowie Übungsaufgaben sind nicht enthalten, können aber angefragt werden und werden dann möglicherweise ausgehändigt.
